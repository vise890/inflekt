use std::env;

fn main() {
    let words = env::args().skip(1).collect();

    inflekt::print_matrix(inflekt::inflect_all(&words));
}
