use std::collections::HashSet;
use std::hash::Hash;

pub struct DedupedVec<T>
    where T: Eq + Hash + Clone {
    added: HashSet<T>,
    pub items: Vec<T>,
}

impl<T> DedupedVec<T>
    where T: Eq + Hash + Clone
{
    pub fn new() -> DedupedVec<T> {
        DedupedVec { added: HashSet::new(), items: Vec::new() }
    }
    pub fn push(&mut self, item: T) {
        if !self.added.contains(&item) {
            self.added.insert(item.clone());
            self.items.push(item);
        }
    }
}
