# Inflekt

Inflects input in a bunch of cases.

Mainly to perform search and replace with tools like
[ripgrep](https://github.com/BurntSushi/ripgrep) or
[fastmod](https://github.com/facebookincubator/fastmod).

## Install

```bash
cargo install inflekt
```

## Usage

```bash
➜ inflekt foo-bar
foobar
FOOBAR
fooBar
FooBar
foo-bar
Foo-Bar
foo_bar
FOO_BAR


➜ inflekt foo-bar beep-boop
foobar beepboop
FOOBAR BEEPBOOP
fooBar beepBoop
FooBar BeepBoop
foo-bar beep-boop
Foo-Bar Beep-Boop
foo_bar beep_boop
FOO_BAR BEEP_BOOP


# interactively replace all inflections of foo-bar with corresponding
# inflection of beep-boop
➜  xargs \
  --arg-file <(inflekt foo-bar beep-boop) \
  --max-lines=1 \
  fastmod --fixed-strings

# grep for all inflections of foo-bar
➜  rg "($(inflekt foo-bar | xargs | sed 's/ /)|(/g'))"

# NOTE: the magic inside the quotes just turns the inflekt output into a regex:
➜  echo "($(./target/debug/inflekt foo-bar | xargs | sed 's/ /)|(/g'))"
(foobar)|(FOOBAR)|(fooBar)|(FooBar)|(foo-bar)|(Foo-Bar)|(foo_bar)|(FOO_BAR)
```
